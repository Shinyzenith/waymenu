# Waymenu:

dmenu clone for wayland written with `gtk`, `gtk-layer-shell` and `zig`.

# Dependencies:

1. `gtk-3.0`
1. `gtk-layer-shell`
1. `wayland-protocols`
1. `wayland`
1. `zig` (0.10.0)

# Building:

```console
$ git clone https://codeberg.org/shinyzenith/waymenu;cd waymenu
# zig build --prefix /usr/local -Drelease-fast
```
