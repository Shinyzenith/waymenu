const gtk = @cImport({
    @cInclude("gtk-layer-shell/gtk-layer-shell.h");
    @cInclude("gtk/gtk.h");
});
const std = @import("std");
const utils = @import("utils.zig");
const callbacks = @import("callbacks.zig");

pub fn main() u8 {
    if (gtk.gtk_layer_is_supported() == 0) {
        std.debug.print("Your compositor may not support gtk-layer-shell.", .{});
    }

    const app = gtk.gtk_application_new("xyz.shinyzenith.waymenu", gtk.G_APPLICATION_FLAGS_NONE);
    defer gtk.g_object_unref(app);

    _ = utils.g_signal_connect(app, "activate", @ptrCast(gtk.GCallback, &callbacks.activate_callback), null);

    const status: i32 = gtk.g_application_run(@ptrCast(*gtk.GApplication, app), 0, null);
    return @intCast(u8, status);
}
