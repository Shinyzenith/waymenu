const std = @import("std");
const utils = @import("utils.zig");
const gtk = @cImport({
    @cInclude("gtk/gtk.h");
    @cInclude("gtk-layer-shell/gtk-layer-shell.h");
});

pub fn activate_callback(app: *gtk.GtkApplication, _: gtk.gpointer) void {
    const window = @ptrCast(*gtk.GtkWindow, gtk.gtk_application_window_new(app));
    gtk.gtk_layer_init_for_window(window);
    gtk.gtk_layer_set_namespace(window, "waymenu");

    gtk.gtk_layer_set_layer(window, gtk.GTK_LAYER_SHELL_LAYER_OVERLAY);
    gtk.gtk_layer_set_exclusive_zone(window, -100);
    //gtk.gtk_layer_set_keyboard_mode(window, gtk.GTK_LAYER_SHELL_KEYBOARD_MODE_EXCLUSIVE);

    gtk.gtk_layer_set_anchor(window, gtk.GTK_LAYER_SHELL_EDGE_LEFT, 1);
    gtk.gtk_layer_set_anchor(window, gtk.GTK_LAYER_SHELL_EDGE_RIGHT, 1);
    gtk.gtk_layer_set_anchor(window, gtk.GTK_LAYER_SHELL_EDGE_TOP, 1);
    gtk.gtk_layer_set_anchor(window, gtk.GTK_LAYER_SHELL_EDGE_BOTTOM, 0);

    const label = gtk.gtk_label_new("waymenu");
    gtk.gtk_label_set_markup(@ptrCast(*gtk.GtkLabel, label), "<span font_desc=\"20.0\">Hello Aakash</span>");

    gtk.gtk_container_add(@ptrCast(*gtk.GtkContainer, window), label);
    gtk.gtk_container_set_border_width(@ptrCast(*gtk.GtkContainer, window), 12);

    _ = utils.g_signal_connect(app, "destroy", @ptrCast(gtk.GCallback, &destroy_callback), null);
    _ = utils.g_signal_connect(app, "changed", @ptrCast(gtk.GCallback, &kb_selected_callback), null);
    gtk.gtk_widget_show_all(@ptrCast(*gtk.GtkWidget, window));
}

fn destroy_callback(_: *gtk.GtkWindow, _: gtk.gpointer) void {
    std.debug.print("Destroy callback called.", .{});
    gtk.gtk_main_quit();
}

fn kb_selected_callback(_: *gtk.GtkComboBox, _: gtk.GtkWindow) void {
    std.debug.print("Callback called.", .{});
}
