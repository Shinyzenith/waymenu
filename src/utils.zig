const gtk = @cImport(@cInclude("gtk/gtk.h"));

// Wrappers over original functions for zig compatiblity.

pub fn g_signal_connect(
    instance: gtk.gpointer,
    detailed_signal: [*c]const gtk.gchar,
    c_handler: gtk.GCallback,
    _: gtk.gpointer,
) gtk.gulong {
    var zero: u32 = 0;
    const flags: *gtk.GConnectFlags = @ptrCast(*gtk.GConnectFlags, &zero);
    return gtk.g_signal_connect_data(instance, detailed_signal, c_handler, null, null, flags.*);
}

pub fn g_signal_connect_swapped(
    instance: gtk.gpointer,
    detailed_signal: [*c]const gtk.gchar,
    c_handler: gtk.GCallback,
    data: gtk.gpointer,
) gtk.gulong {
    return gtk.g_signal_connect_data(instance, detailed_signal, c_handler, data, null, gtk.G_CONNECT_SWAPPED);
}
